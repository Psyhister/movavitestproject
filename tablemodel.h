#ifndef TABLEMODEL_H
#define TABLEMODEL_H

#include <QAbstractTableModel>
#include <QList>
#include <QDateTime>
#include "directorywatcher.h"

/* This class is based on this example from Qt:
 * http://doc.qt.io/qt-5/qtwidgets-itemviews-addressbook-tablemodel-h.html
 * http://doc.qt.io/qt-5/qtwidgets-itemviews-addressbook-tablemodel-cpp.html
 */

class TableModel : public QAbstractTableModel
{
    Q_OBJECT

public:
    TableModel(QObject *parent = 0);
    TableModel(QList<QPair<QString, QString> > listofPairs, QObject *parent = 0);

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    int columnCount(const QModelIndex &parent = QModelIndex()) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    Qt::ItemFlags flags(const QModelIndex &index) const override;
    bool setData(int row, int column, const QVariant &value, int role = Qt::EditRole);
    bool insertRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
    bool removeRows(int position, int rows, const QModelIndex &index = QModelIndex()) override;
private:
    struct FileData {
        QString name;
        size_t size;
        QDateTime date;
    };

    QList<FileData> list;
    QString humanSize(size_t) const;
};

#endif // TABLEMODEL_H
