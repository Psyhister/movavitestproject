#include <windows.h>
#include <strsafe.h>
#include <algorithm>
#include "DirectoryWatcher.h"

namespace {
    constexpr int MAX_DIRS{ 10 };
    constexpr int MAX_FILES{ 255 };
    constexpr int MAX_BUFFER{ 4096 };
}

#ifdef UNICODE
using strtype = std::wstring;
#else
using strtype = std::string;
#endif // UNICODE

strtype convert_string(const std::string& s) {
#ifdef UNICODE
    WCHAR c[MAX_PATH];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), -1, c, MAX_PATH);
    return strtype(c);
#else
    return s;
#endif // UNICODE
}

std::string convert_to_string(const strtype& s) {
#ifdef UNICODE
    char c[MAX_PATH * 2];
    WideCharToMultiByte(CP_ACP, 0, s.c_str(), -1, c, MAX_PATH * 2, NULL, NULL);
    return std::string(c);
#else
    return s;
#endif // UNICODE

}

time_t convert_time(const FILETIME& ft) {
    ULARGE_INTEGER ull;
    ull.LowPart = ft.dwLowDateTime;
    ull.HighPart = ft.dwHighDateTime;
    return ull.QuadPart / 10000000ULL - 11644473600ULL;
}

size_t convert_size(const WIN32_FIND_DATA& fd) {
    LARGE_INTEGER filesize;
    filesize.LowPart = fd.nFileSizeLow;
    filesize.HighPart = fd.nFileSizeHigh;
    return static_cast<size_t>(filesize.QuadPart);
}

class DirectoryWatcher::DirectoryWatcherImpl {
    typedef struct _DIRECTORY_INFO {
        HANDLE      hDir = nullptr;
        TCHAR       lpszDirName[MAX_PATH];
        CHAR        lpBuffer[MAX_BUFFER];
        DWORD       dwBufLength;
        OVERLAPPED  Overlapped;
    }DIRECTORY_INFO, *PDIRECTORY_INFO, *LPDIRECTORY_INFO;

    std::function<void(FileData)> m_addFileHandler;
    std::function<void(const std::string &)> m_delFileHandler;
    std::function<void(const std::string&, size_t, size_t)> m_changeSizeHandler;
    std::function<void(const std::string&, const std::string&)> m_changeNameHandler;
    std::vector<FileData> m_fileList;
    DIRECTORY_INFO m_dirInfo;
    HANDLE m_hCompPort = nullptr;
    HANDLE m_hThread = nullptr;

    // WinAPI can give us information only about one name at the same time and documentation doesn't give
    // consise information whether old name is sent first or not, so I've decided to err on the safe side
    // and save both names and after we have both, do the update
    strtype m_oldName;
    strtype m_newName;

    void fillFileList();

    void addFile(const strtype&);
    void delFile(const strtype&);
    void modFile(const strtype&);
    void renameFile(const strtype&, const strtype&);
    WIN32_FIND_DATA m_readFile(const strtype&);

    // WinAPI was designed with C in mind and does not allow to use non-static member methods as callbacks
    // for its threads, so we have to use this cludge
    static void WINAPI threadHandler(LPVOID param) {
        DirectoryWatcher::DirectoryWatcherImpl *obj = reinterpret_cast<DirectoryWatcher::DirectoryWatcherImpl*>(param);
        obj->directoryChangeHandler();
    }

    void closeHandlers();
public:
    ~DirectoryWatcherImpl();
    void directoryChangeHandler();
    void watchDirectory(const std::string& path);
    std::vector<FileData> getFileList() const;
    void setAddFileHandler(const std::function<void(FileData)> &handler) {
        m_addFileHandler = handler;
    }
    void setDeleteFileHandler(const std::function<void(const std::string&)>& handler) {
        m_delFileHandler = handler;
    }
    void setChangeFileSizeHandler(const std::function<void(const std::string&,size_t,size_t)>& handler) {
        m_changeSizeHandler = handler;
    }
    void setChangeFileNameHandler(const std::function<void(const std::string&,const std::string&)>& handler) {
        m_changeNameHandler = handler;
    }
};

DirectoryWatcher::DirectoryWatcherImpl::~DirectoryWatcherImpl() {
    closeHandlers();
}

void DirectoryWatcher::DirectoryWatcherImpl::closeHandlers() {
    if (m_hThread != nullptr) {
        PostQueuedCompletionStatus(m_hCompPort, 0, 0, NULL);
        // Wait for the Directory thread to finish before exiting
        WaitForSingleObject(m_hThread, INFINITE);
        CloseHandle(m_hThread);
    }

    if (m_dirInfo.hDir != nullptr)
        CloseHandle(m_dirInfo.hDir);
    if (m_hCompPort != nullptr)
        CloseHandle(m_hCompPort);
}

void DirectoryWatcher::DirectoryWatcherImpl::watchDirectory(const std::string& path) {
    strtype s_path = convert_string(path);
    closeHandlers();
    m_dirInfo.hDir = CreateFile(s_path.c_str(),
        FILE_LIST_DIRECTORY,
        FILE_SHARE_READ |
        FILE_SHARE_WRITE |
        FILE_SHARE_DELETE,
        NULL,
        OPEN_EXISTING,
        FILE_FLAG_BACKUP_SEMANTICS |
        FILE_FLAG_OVERLAPPED,
        NULL);

    if (m_dirInfo.hDir == INVALID_HANDLE_VALUE) {
        throw std::runtime_error("Failed to open the directory");
    }
    StringCchCopy(m_dirInfo.lpszDirName, MAX_PATH, s_path.c_str());

    m_hCompPort = CreateIoCompletionPort(m_dirInfo.hDir,
        m_hCompPort,
        (DWORD)&m_dirInfo,
        0);

    DWORD   tid;

    ReadDirectoryChangesW(m_dirInfo.hDir,
        m_dirInfo.lpBuffer,
        MAX_BUFFER,
        FALSE,
        FILE_NOTIFY_CHANGE_LAST_WRITE |
        FILE_NOTIFY_CHANGE_FILE_NAME,
        &m_dirInfo.dwBufLength,
        &m_dirInfo.Overlapped,
        NULL);

    m_hThread = CreateThread(NULL,
        0,
        (LPTHREAD_START_ROUTINE)threadHandler,
        (LPVOID)this,
        0,
        &tid);
    fillFileList();
}

void DirectoryWatcher::DirectoryWatcherImpl::fillFileList() {
    WIN32_FIND_DATA ffd;
    TCHAR szDir[MAX_PATH];
    HANDLE hFind = INVALID_HANDLE_VALUE;

    StringCchCopy(szDir, MAX_PATH, m_dirInfo.lpszDirName);
    StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

    // Iterate over files in the directory, one by one
    m_fileList.clear();
    hFind = FindFirstFile(szDir, &ffd);
    do {
        if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            m_fileList.push_back({ convert_to_string(ffd.cFileName), convert_time(ffd.ftLastWriteTime), 4096, true });
        }
        else {
            m_fileList.push_back({convert_to_string(ffd.cFileName), convert_time(ffd.ftLastWriteTime), convert_size(ffd), false});
        }
    } while (FindNextFile(hFind, &ffd) != 0);
}

WIN32_FIND_DATA DirectoryWatcher::DirectoryWatcherImpl::m_readFile(const strtype &fileName) {
    TCHAR szDir[MAX_PATH];
    StringCchCopy(szDir, MAX_PATH, m_dirInfo.lpszDirName);
    StringCchCat(szDir, MAX_PATH, TEXT("\\"));
    StringCchCat(szDir, MAX_PATH, fileName.c_str());
    WIN32_FIND_DATA ffd;
    FindFirstFile(szDir, &ffd);
    return ffd;
}

void DirectoryWatcher::DirectoryWatcherImpl::addFile(const strtype &fileName) {
    WIN32_FIND_DATA ffd = m_readFile(fileName);
    m_fileList.push_back({convert_to_string(ffd.cFileName), convert_time(ffd.ftLastWriteTime), convert_size(ffd), false});
    m_addFileHandler(m_fileList.back());
}

void DirectoryWatcher::DirectoryWatcherImpl::delFile(const strtype &fileName) {
    std::string s_fileName = convert_to_string(fileName);
    m_fileList.erase(std::remove_if(m_fileList.begin(), m_fileList.end(), [=](FileData& d) { return d.name == s_fileName; }), m_fileList.end());
    m_delFileHandler(s_fileName);
}

void DirectoryWatcher::DirectoryWatcherImpl::modFile(const strtype &fileName) {
    WIN32_FIND_DATA ffd = m_readFile(fileName);

    std::string s_fileName = convert_to_string(fileName);
    auto it = std::find_if(m_fileList.begin(), m_fileList.end(), [&](FileData d) {return d.name == s_fileName; });
    time_t oldModTime = it->lastModified, newModTime = convert_time(ffd.ftLastAccessTime);
    size_t oldSize = it->size, newSize = convert_size(ffd);

    if (oldModTime != newModTime) {
        it->lastModified = newModTime;
    }

    if (oldSize != newSize) {
        it->size = newSize;
        m_changeSizeHandler(s_fileName, oldSize, newSize);
    }
}

void DirectoryWatcher::DirectoryWatcherImpl::renameFile(const strtype &oldName, const strtype &newName) {
    std::string s_oldName = convert_to_string(oldName), s_newName = convert_to_string(newName);
    for (auto &fd : m_fileList) {
        if (fd.name == s_oldName) {
            fd.name = s_newName;
            break;
        }
    }
    m_changeNameHandler(s_oldName, s_newName);
}

void DirectoryWatcher::DirectoryWatcherImpl::directoryChangeHandler() {
    DWORD numBytes;
    DWORD cbOffset;
    LPDIRECTORY_INFO di;
    LPOVERLAPPED lpOverlapped;
    PFILE_NOTIFY_INFORMATION fni;

    do {
        // Retrieve the directory info for this directory
        // through the completion key
        GetQueuedCompletionStatus(m_hCompPort,
            &numBytes,
            (LPDWORD)&di,      // This is the DIRECTORY_INFO structure that was passed in the call to CreateIoCompletionPort below.
            &lpOverlapped,
            INFINITE);

        if (di) {
            fni = (PFILE_NOTIFY_INFORMATION)di->lpBuffer;

            do {
                cbOffset = fni->NextEntryOffset;

                TCHAR fileName[MAX_PATH];
                StringCchCopy(fileName, fni->FileNameLength / sizeof(TCHAR) + 1, fni->FileName);
                fileName[fni->FileNameLength / sizeof(TCHAR) + 1] = '\0';

                switch (fni->Action) {
                case FILE_ACTION_ADDED:
                    addFile(fileName);
                    break;
                case FILE_ACTION_REMOVED:
                    delFile(fileName);
                    break;
                case FILE_ACTION_MODIFIED:
                    modFile(fileName);
                    break;
                case FILE_ACTION_RENAMED_OLD_NAME:
                    m_oldName = fileName;
                    break;
                case FILE_ACTION_RENAMED_NEW_NAME:
                    m_newName = fileName;
                    break;
                default: wprintf(L"unknown event: ");
                    break;
                }
                if (!m_oldName.empty() && !m_newName.empty()) {
                    renameFile(m_oldName, m_newName);
                    m_oldName.clear();
                    m_newName.clear();
                }

                //

                fni = (PFILE_NOTIFY_INFORMATION)((LPBYTE)fni + cbOffset);

            } while (cbOffset);

            // Reissue the watch command
            ReadDirectoryChangesW(di->hDir, di->lpBuffer,
                MAX_BUFFER,
                TRUE,
                FILE_NOTIFY_CHANGE_LAST_WRITE,
                &di->dwBufLength,
                &di->Overlapped,
                NULL);
        }

    } while (di);
}

std::vector<DirectoryWatcher::FileData> DirectoryWatcher::DirectoryWatcherImpl::getFileList() const {
    return m_fileList;
}

DirectoryWatcher::DirectoryWatcher() {
    pimpl = std::make_unique<DirectoryWatcher::DirectoryWatcherImpl>();
}

DirectoryWatcher::~DirectoryWatcher(){}

void DirectoryWatcher::watchDirectory(const std::string& path){
    pimpl->watchDirectory(path);
}

std::vector<DirectoryWatcher::FileData> DirectoryWatcher::getFileList() const {
    return pimpl->getFileList();
}

void DirectoryWatcher::setAddFileHandler(const std::function<void(FileData)>& handler) {
    pimpl->setAddFileHandler(handler);
}

void DirectoryWatcher::setDeleteFileHandler(const std::function<void(const std::string&)>& handler) {
    pimpl->setDeleteFileHandler(handler);
}

void DirectoryWatcher::setChangeFileSizeHandler(const std::function<void(const std::string&,size_t,size_t)>& handler) {
    pimpl->setChangeFileSizeHandler(handler);
}

void DirectoryWatcher::setChangeFileNameHandler(const std::function<void(const std::string&,const std::string&)>& handler) {
    pimpl->setChangeFileNameHandler(handler);
}
