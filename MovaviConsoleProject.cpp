// MovaviConsoleProject.cpp : Defines the entry point for the console application.
//

#include "DirectoryWatcher.h"
#include <conio.h>
#include <iostream>

int main()
{
	DirectoryWatcher watcher;
	watcher.watchDirectory(L"C:\\adb\\123455");
	watcher.setAddFileHandler([=](DirectoryWatcher::FileData fd) {
		wprintf(L"Name: %s, size: %d, last modified: %lld", fd.name, fd.size, fd.lastModified);
	});
	watcher.setDeleteFileHandler([=](const strtype& s) {
		wprintf(L"Name: %s", s.c_str());
	});
	watcher.setChangeFileSizeHandler([=](const strtype &s, size_t size1, size_t size2) {
		wprintf(L"Name: %s, old: %d, new: %d\n", s.c_str(), size1, size2);
	});
	watcher.setChangeFileNameHandler([=](const strtype &s1, const strtype &s2) {
		wprintf(L"Old: %s, new: %s", s1.c_str(), s2.c_str());
	});
	while (_getch() != 'q');
    return 0;
}

