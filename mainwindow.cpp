#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qfiledialog.h"

using DWFileData = DirectoryWatcher::FileData;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    model = std::make_unique<TableModel>();
    ui->tableView->setModel(model.get());
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    dw.setAddFileHandler([this](DWFileData fd) {
        size_t index = list.size();
        list.push_back(fd);
        model->insertRows(model->rowCount(QModelIndex()), 1, QModelIndex());
        model->setData(index, 0, QVariant(fd.name.c_str()), Qt::EditRole);
        model->setData(index, 1, QVariant(fd.size), Qt::EditRole);
        model->setData(index, 2, QDateTime::fromTime_t(fd.lastModified), Qt::EditRole);
    });

    dw.setChangeFileNameHandler([this](const std::string &oldName, const std::string &newName){
        // std::find didn't work for some reason, had to improvise
        auto it = this->findData(oldName);
        if (it != list.end()) {
            it->name = newName;
            model->setData(std::distance(list.begin(), it), 0, QVariant(newName.c_str()));
        }
    });

    dw.setChangeFileSizeHandler([this](const std::string &name, size_t oldSize, size_t newSize){
        Q_UNUSED(oldSize);
        auto it = this->findData(name);
        if (it != list.end()) {
            it->size = newSize;
            model->setData(std::distance(list.begin(), it), 1, QVariant(newSize));
        }
    });

    dw.setDeleteFileHandler([this](const std::string &name) {
        auto it = this->findData(name);
        if (it != list.end()) {
            model->removeRows(std::distance(list.begin(), it), 1, QModelIndex());
            list.erase(it);
        }
    });
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::on_browse_clicked() {
    QString str = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                     "C:\\",
                                                     QFileDialog::ShowDirsOnly
                                                     | QFileDialog::DontResolveSymlinks);
    ui->lineEdit->setText(str);
    dw.watchDirectory(str.toStdString());
    list = dw.getFileList();
    model->removeRows(0, model->rowCount());
    model->insertRows(model->rowCount(QModelIndex()), list.size());
    for (size_t i = 0; i < list.size(); i++) {
        model->setData(i, 0, QVariant(list[i].name.c_str()), Qt::EditRole);
        model->setData(i, 1, QVariant(list[i].size), Qt::EditRole);
        model->setData(i, 2, QDateTime::fromTime_t(list[i].lastModified), Qt::EditRole);
    }
}

std::vector<DWFileData>::iterator MainWindow::findData(const std::string &name) {
    auto it = list.begin();
    for (; it != list.end(); it++) {
        if (it->name == name)
            break;
    }
    return it;
}
