#include "tablemodel.h"

TableModel::TableModel(QObject *parent)
    : QAbstractTableModel(parent)
{
}

int TableModel::rowCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return list.size();
}

int TableModel::columnCount(const QModelIndex &parent) const {
    Q_UNUSED(parent);
    return 3;
}

QVariant TableModel::data(const QModelIndex &index, int role) const {
    if (!index.isValid())
        return QVariant();

    if (index.row() >= list.size() || index.row() < 0)
        return QVariant();

    if (role == Qt::DisplayRole) {
        FileData fd = list.at(index.row());

        if (index.column() == 0)
            return fd.name;
        else if (index.column() == 1)
            return humanSize(fd.size);
        else if (index.column() == 2)
            return fd.date.toString(Qt::SystemLocaleShortDate);
    }
    return QVariant();
}

QVariant TableModel::headerData(int section, Qt::Orientation orientation, int role) const {
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal) {
        switch (section) {
            case 0:
                return tr("Name");
            case 1:
                return tr("Size");
            case 2:
                return tr("Last Modified");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

bool TableModel::insertRows(int position, int rows, const QModelIndex &index) {
    Q_UNUSED(index);
    beginInsertRows(QModelIndex(), position, position + rows - 1);
    for (int row = 0; row < rows; ++row) {
        FileData fd {QString(), 0, QDateTime()};
        list.insert(position, fd);
    }

    endInsertRows();
    return true;
}

bool TableModel::removeRows(int position, int rows, const QModelIndex &index) {
    Q_UNUSED(index);
    if (rowCount() < 1 || position + rows > rowCount()) return false;
    beginRemoveRows(QModelIndex(), position, position + rows - 1);

    for (int row = 0; row < rows; ++row) {
        list.removeAt(position);
    }

    endRemoveRows();
    return true;
}

QString TableModel::humanSize(size_t size) const {
    QStringList list;
    list << "KB" << "MB" << "GB" << "TB";

    QStringListIterator i(list);
    QString unit("bytes");

    while(size >= 1024 && i.hasNext()) {
        unit = i.next();
        size /= 1024;
    }
    return QString().setNum(size)+" "+unit;
}

bool TableModel::setData(int row, int column, const QVariant &value, int role) {
    QModelIndex index = this->index(row, column);
    if (index.isValid() && role == Qt::EditRole) {
        int row = index.row();


        FileData fd = list.value(row);

        if (index.column() == 0)
            fd.name = value.toString();
        else if (index.column() == 1)
            fd.size = value.toUInt();
        else if (index.column() == 2) {
            fd.date = value.toDateTime();
        }
        else
            return false;

        list.replace(row, fd);
        emit(dataChanged(index, index));

        return true;
    }

    return false;
}

Qt::ItemFlags TableModel::flags(const QModelIndex &index) const {
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    return QAbstractTableModel::flags(index);
}
