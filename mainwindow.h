#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>
#include "directorywatcher.h"
#include "tablemodel.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_browse_clicked();

private:
    void onFileNameChange(const std::string &oldName, const std::string &newName);
    Ui::MainWindow *ui;
    DirectoryWatcher dw;
    std::unique_ptr<TableModel> model;
    std::vector<DirectoryWatcher::FileData> list;
    std::vector<DirectoryWatcher::FileData>::iterator findData(const std::string&);
};

#endif // MAINWINDOW_H
