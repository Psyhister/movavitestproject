#pragma once
#include <string>
#include <memory>
#include <vector>
#include <functional>

class DirectoryWatcher
{
public:
    struct FileData {
        std::string name;
        time_t lastModified;
        size_t size;
        bool is_directory;
    };

    DirectoryWatcher();
    ~DirectoryWatcher();

    void watchDirectory(const std::string& path);
    std::vector<FileData> getFileList() const;

    void setAddFileHandler(const std::function<void(FileData)>& handler);
    void setDeleteFileHandler(const std::function<void(const std::string&)>& handler);
    void setChangeFileSizeHandler(const std::function<void(const std::string&,size_t,size_t)>& handler);
    void setChangeFileNameHandler(const std::function<void(const std::string&,const std::string&)>& handler);
private:
    class DirectoryWatcherImpl;

    std::unique_ptr<DirectoryWatcherImpl> pimpl;
};

